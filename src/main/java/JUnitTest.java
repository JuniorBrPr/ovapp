import json.Json;
import json.User;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class JUnitTest {

        @Test
        public void testJSonAddUser() {
            var testUser = new User();
            testUser.setUserName("jUnitTest");
            Json json = new Json();

            json.addUser(testUser);
            assertEquals(json.getUser(json.getUsers().size()-1).getUserName(),"jUnitTest");
        }
    }
