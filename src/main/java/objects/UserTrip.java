package objects;

public class UserTrip {
    private int tripId;
    private String departure;
    private String arrival;
    private String modeOfTransport;

    public String getModeOfTransport() {
        return modeOfTransport;
    }

    public void setModeOfTransport(String modeOfTransport) {
        this.modeOfTransport = modeOfTransport;
    }

    public UserTrip() {

    }

    public UserTrip(int tripId, String departure, String arrival, String modeOfTransport) {
        this.tripId = tripId;
        this.departure = departure;
        this.arrival = arrival;
        this.modeOfTransport = modeOfTransport;
    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public String getDeparture() {
        return departure;
    }

    public void setDeparture(String departure) {
        this.departure = departure;
    }

    public String getArrival() {
        return arrival;
    }

    public void setArrival(String arrival) {
        this.arrival = arrival;
    }
}


