package objects;

import objects.Data;
import objects.Location;
import objects.Route;

import java.time.LocalTime;

public class BusData extends Data
{
    public BusData() {
        Location location = new Location("station Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Stationsstraat, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Dorpsstraat, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Kerkstraat, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Landgoed Schovenhorst, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("School Shovenhorst, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Centrum, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Bosrand, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Landgoed Groot Spriel, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("De Hertshoorn, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("De Rusthoeve, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Heidenheuvel, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("West End, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Hooiweg, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Busstation Wittenberg, Stroe");
        locationMap.put(location.getName(), location);
        location = new Location("Hoeve Oud Milligen, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Ouwendroperweg, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Koningklijke Luchtmacht, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Kruispunt, Uddel");
        locationMap.put(location.getName(), location);
        location = new Location("Ouwendroperweg, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Koningklijke Luchtmacht, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Het Aardhuis, Hoog Soeren");
        locationMap.put(location.getName(), location);
        location = new Location("Oranjeoord, Hoog Soeren");
        locationMap.put(location.getName(), location);
        location = new Location("Echoput, Hoog Soeren");
        locationMap.put(location.getName(), location);
        location = new Location("Koningin Julianatoren, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Paleis het Loo, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Gedenknaald, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Bosweg, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Grote Kerk, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Oranjepark, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Marktplein, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Station Apeldoorn, Apeldoorn");
        locationMap.put(location.getName(), location);
        location = new Location("Halte Eemplein, Amersfoort");
        locationMap.put(location.getName(), location);
        location = new Location("Halte stadhuis, Amersfoort");
        locationMap.put(location.getName(), location);
        location = new Location("Amersfoort centraal, Amersfoort");
        locationMap.put(location.getName(), location);
        location = new Location("Mooi Veluwe, Putten");
        locationMap.put(location.getName(), location);
        location = new Location("Veluw Hul, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("De Hertshoorn, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Bakkerstraat, Garderen");
        locationMap.put(location.getName(), location);
        location = new Location("Halte Snouckeartlaan, Amersfoort");
        locationMap.put(location.getName(), location);



        for (int hour = 7; hour <= 19; hour += 1) {

            //Putten-----Apeldoorn
            var route = new Route(locationMap.get("station Putten"), LocalTime.of(hour, 0));
            route.addStopOver(locationMap.get("Stationsstraat, Putten"), LocalTime.of(hour, 2), LocalTime.of(hour, 2));
            route.addStopOver(locationMap.get("Dorpsstraat, Putten"), LocalTime.of(hour, 6), LocalTime.of(hour, 6));
            route.addStopOver(locationMap.get("Kerkstraat, Putten"), LocalTime.of(hour, 8), LocalTime.of(hour, 8));
            route.addStopOver(locationMap.get("Centrum, Putten"), LocalTime.of(hour, 10), LocalTime.of(hour, 10));
            route.addStopOver(locationMap.get("Bosrand, Putten"), LocalTime.of(hour, 11), LocalTime.of(hour, 11));
            route.addStopOver(locationMap.get("Landgoed Schovenhorst, Putten"), LocalTime.of(hour, 11), LocalTime.of(hour, 11));
            route.addStopOver(locationMap.get("School Shovenhorst, Putten"), LocalTime.of(hour, 13), LocalTime.of(hour, 13));
            route.addStopOver(locationMap.get("Landgoed Groot Spriel, Putten"), LocalTime.of(hour, 14), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("Mooi Veluwe, Putten"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("De Rusthoeve, Putten"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Veluw Hul, Garderen"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("De Hertshoorn, Garderen"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Heidenheuvel, Garderen"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("West End, Garderen"), LocalTime.of(hour, 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Bakkerstraat, Garderen"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Hooiweg, Garderen"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Busstation Wittenberg, Stroe"), LocalTime.of(hour, 22), LocalTime.of(hour, 27));

            //bus pauze
            route.addStopOver(locationMap.get("Hoeve Oud Milligen, Garderen"), LocalTime.of(hour, 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("Ouwendroperweg, Garderen"), LocalTime.of(hour, 29), LocalTime.of(hour, 29));
            route.addStopOver(locationMap.get("Koningklijke Luchtmacht, Garderen"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Kruispunt, Uddel"), LocalTime.of(hour, 32), LocalTime.of(hour, 32));
            route.addStopOver(locationMap.get("Het Aardhuis, Hoog Soeren"), LocalTime.of(hour, 37), LocalTime.of(hour, 37));
            route.addStopOver(locationMap.get("Oranjeoord, Hoog Soeren"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Echoput, Hoog Soeren"), LocalTime.of(hour, 39), LocalTime.of(hour, 39));
            route.addStopOver(locationMap.get("Koningin Julianatoren, Apeldoorn"), LocalTime.of(hour, 43), LocalTime.of(hour, 43));
            route.addStopOver(locationMap.get("Paleis het Loo, Apeldoorn"), LocalTime.of(hour, 45), LocalTime.of(hour, 45));
            route.addStopOver(locationMap.get("Gedenknaald, Apeldoorn"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Bosweg, Apeldoorn"), LocalTime.of(hour, 47), LocalTime.of(hour, 47));
            route.addStopOver(locationMap.get("Grote Kerk, Apeldoorn"), LocalTime.of(hour, 48), LocalTime.of(hour, 48));
            route.addStopOver(locationMap.get("Oranjepark, Apeldoorn"), LocalTime.of(hour, 49), LocalTime.of(hour, 49));
            route.addStopOver(locationMap.get("Marktplein, Apeldoorn"), LocalTime.of(hour, 50), LocalTime.of(hour, 50));
            route.addEndPoint(locationMap.get("Station Apeldoorn, Apeldoorn"), LocalTime.of(hour, 53));
            addRoute(route);
        }

        for (int hour = 7; hour <= 19; hour += 1) {
            //Apeldoorn Putten
            var route = new Route(locationMap.get("Station Apeldoorn, Apeldoorn"), LocalTime.of(hour, 0));
            route.addStopOver(locationMap.get("Marktplein, Apeldoorn"), LocalTime.of(hour, 2), LocalTime.of(hour, 2));
            route.addStopOver(locationMap.get("Oranjepark, Apeldoorn"), LocalTime.of(hour, 6), LocalTime.of(hour, 6));
            route.addStopOver(locationMap.get("Grote Kerk, Apeldoorn"), LocalTime.of(hour, 8), LocalTime.of(hour, 8));
            route.addStopOver(locationMap.get("Bosweg, Apeldoorn"), LocalTime.of(hour, 10), LocalTime.of(hour, 10));
            route.addStopOver(locationMap.get("Gedenknaald, Apeldoorn"), LocalTime.of(hour, 11), LocalTime.of(hour, 11));
            route.addStopOver(locationMap.get("Paleis het Loo, Apeldoorn"), LocalTime.of(hour, 11), LocalTime.of(hour, 11));
            route.addStopOver(locationMap.get("Koningin Julianatoren, Apeldoorn"), LocalTime.of(hour, 13), LocalTime.of(hour, 13));
            route.addStopOver(locationMap.get("Echoput, Hoog Soeren"), LocalTime.of(hour, 14), LocalTime.of(hour, 14));
            route.addStopOver(locationMap.get("Oranjeoord, Hoog Soeren"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Het Aardhuis, Hoog Soeren"), LocalTime.of(hour, 15), LocalTime.of(hour, 15));
            route.addStopOver(locationMap.get("Kruispunt, Uddel"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Koningklijke Luchtmacht, Garderen"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("De Hertshoorn, Garderen"), LocalTime.of(hour, 17), LocalTime.of(hour, 17));
            route.addStopOver(locationMap.get("Heidenheuvel, Garderen"), LocalTime.of(hour, 18), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Ouwendroperweg, Garderen"), LocalTime.of(hour, 19), LocalTime.of(hour, 19));
            route.addStopOver(locationMap.get("Hoeve Oud Milligen, Garderen"), LocalTime.of(hour, 20), LocalTime.of(hour, 20));
            route.addStopOver(locationMap.get("Busstation Wittenberg, Stroe"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));

            //Bus pauze
            route.addStopOver(locationMap.get("Busstation Wittenberg, Stroe"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Hooiweg, Garderen"), LocalTime.of(hour, 22), LocalTime.of(hour, 22));
            route.addStopOver(locationMap.get("Bakkerstraat, Garderen"), LocalTime.of(hour, 26), LocalTime.of(hour, 26));
            route.addStopOver(locationMap.get("Kerkstraat, Putten"), LocalTime.of(hour, 28), LocalTime.of(hour, 28));
            route.addStopOver(locationMap.get("West End, Garderen"), LocalTime.of(hour, 30), LocalTime.of(hour, 30));
            route.addStopOver(locationMap.get("Heidenheuvel, Garderen"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("De Hertshoorn, Garderen"), LocalTime.of(hour, 31), LocalTime.of(hour, 31));
            route.addStopOver(locationMap.get("Veluw Hul, Garderen"), LocalTime.of(hour, 33), LocalTime.of(hour, 33));
            route.addStopOver(locationMap.get("De Rusthoeve, Putten"), LocalTime.of(hour, 34), LocalTime.of(hour, 34));
            route.addStopOver(locationMap.get("Mooi Veluwe, Putten"), LocalTime.of(hour, 35), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Landgoed Groot Spriel, Putten"), LocalTime.of(hour, 37), LocalTime.of(hour, 37));
            route.addStopOver(locationMap.get("School Shovenhorst, Putten"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Landgoed Schovenhorst, Putten"), LocalTime.of(hour, 38), LocalTime.of(hour, 38));
            route.addStopOver(locationMap.get("Bosrand, Putten"), LocalTime.of(hour, 39), LocalTime.of(hour, 39));
            route.addStopOver(locationMap.get("Centrum, Putten"), LocalTime.of(hour, 40), LocalTime.of(hour, 40));
            route.addStopOver(locationMap.get("Kerkstraat, Putten"), LocalTime.of(hour, 41), LocalTime.of(hour, 41));
            route.addStopOver(locationMap.get("Dorpsstraat, Putten"), LocalTime.of(hour, 42), LocalTime.of(hour, 42));
            route.addStopOver(locationMap.get("Stationsstraat, Putten"), LocalTime.of(hour, 44), LocalTime.of(hour, 44));
            route.addEndPoint(locationMap.get("station Putten"), LocalTime.of(hour, 48));
            addRoute(route);
        }

        for (int hour = 7; hour <= 19; hour += 1) {
                //HU Amersfoort - Amersfoort centraal
                var route = new Route(locationMap.get("Halte Eemplein, Amersfoort"), LocalTime.of(hour, 0));
                route.addStopOver(locationMap.get("Halte stadhuis, Amersfoort"), LocalTime.of(hour, 2), LocalTime.of(hour, 2));
                route.addStopOver(locationMap.get("Halte Snouckeartlaan, Amersfoort"), LocalTime.of(hour, 4), LocalTime.of(hour, 4));
                route.addEndPoint(locationMap.get("Amersfoort centraal, Amersfoort"), LocalTime.of(hour, 6));
                addRoute(route);

        }
    }
}
