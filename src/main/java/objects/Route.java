package objects;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;

public class Route {

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //Declarations
    private final ArrayList<StopOver> stopOvers = new ArrayList<>();

    public ArrayList<StopOver> getStopOversArray() {
        return stopOvers;
    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    Route(){
    }

    //Constructor
    Route(Location location, LocalTime departure) {
        var stopover = new StopOver(location.getName(), /*LocalTime.MAX*/  departure.minusMinutes(4), departure);
        stopOvers.add(stopover);
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Maakt een halte aan
    public void addStopOver(Location location, LocalTime arrival, LocalTime departure) {
        var stopover = new StopOver(location.getName(), arrival, departure);
        stopOvers.add(stopover);
    }


    //Gets amount of stopOvers between departure and arrival points
    public int getStopOvers(String departure, String arrival){
        int start;
        int end;
        int stops=0;

        for (start = 0; start < stopOvers.size(); start++) {
            if (departure.equals(stopOvers.get(start).name)) {
                for (end = start; end < stopOvers.size(); end++) {
                    if (arrival.equals(stopOvers.get(end).name)) {

                        stops=end-start-1;
                    }
                }
            }
        }
        return stops;
    }

    //Maakt een eind halte aan
    public void addEndPoint(Location location, LocalTime arrival) {
        var stopover = new StopOver(location.getName(), arrival, arrival);
        stopOvers.add(stopover);
    }

    public String getStartPoint(){
        String startPoint = stopOvers.get(0).getName();
        return startPoint;
    }

    public String getEndPoint(){
        String endpoint = stopOvers.get(stopOvers.size()-1).getName();
        return endpoint;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    //Maakt de objects.Route naam
    public String getKey() {
        String key = stopOvers.get(0).getName();

        for (int i = 0; i < stopOvers.size(); i++) {
            key += "-";
            key += stopOvers.get(i).getName();
        }
        return key;
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public LocalTime getDeparture(){return stopOvers.get(0).getDeparture();}

    public LocalTime getDeparture(Location location)
    {
        for (var e: stopOvers)
        {
            if (e.getName().equals(location.getName()))
            {
                return e.getDeparture();
            }
        }
        return null;
    }

    public LocalTime getArrival() {
        return stopOvers.get(stopOvers.size() -1).getArrival();
    }

    public LocalTime getArrival(Location locationB)
    {
    return getArrival();
    }
}

