package screens;

import json.Json;
import json.User;
import objects.*;
import org.jxmapviewer.JXMapKit;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.List;

public class TabbedPanel {
    JTabbedPane mainPanel;
    JPanel login;
    JPanel reisplanner;
    JXMapKit map = new JXMapKit();
    private JPanel      panel;
    private JButton     zoeken;
    private JComboBox   aankomstBox;
    private JComboBox   vertrekBox;
    public  String      departureTimeSearch;
    private JLabel      reisAdvies = new JLabel();
    private  JSpinner    timeSpinner;
    private  JSpinner    utimeSpinner;
    private Data data = new Data();
    private TrainData trainData= new TrainData();
    private BusData busData = new BusData();
    private JPanel      tripsPanel;
    private JScrollPane scrollPane;
    private ButtonGroup group = new ButtonGroup();
    private JLabel      selectTransport = new JLabel();
    private Json json = new Json();
    private boolean     tableVisible = false;
    private boolean     stationInfo = true;
    private JButton     mapButton = new JButton();
    //Login
    private JTextField t1, t2;
    private JButton b1, b2;
    private JLabel l1, l2;
    private JPanel loginScreen;
    private JPanel signUpScreen;
    private JPanel loginScreenView;
    private JPanel accountScreen;
    public int userId = 0;
    private boolean matched = false;
    //SignUp
    JTextField sT1, sT2, sT3;
    JButton sB1, sB2;
    JLabel sL3,sL4;
    //Account
    User user;
    Border black = BorderFactory.createLineBorder(Color.BLACK);
    //Reisplanner
    private JLabel rL;
    private JScrollPane userTripsScrollPanel;
    private JPanel uTripsPanel;
    private JPanel reisplannerView;
    private LocalTime localTimeD;
    private String arrivalSearch;
    private String departureSearch;
    Locale baselocale = (new Locale("nl", "NL"));
    ResourceBundle bundle = ResourceBundle.getBundle("bundle", baselocale);
    String userSearch = "";
    JButton search;
    Color backGroundColor = new Color(184, 92, 24);
    Color backGroundColorB = new Color(154, 210, 240);
    Color backGroundColorC = new Color(200, 189, 187);
    Border thickColoredBorder = BorderFactory.createLineBorder(backGroundColor, 10);

    JPanel tripScrollContainer = new JPanel();
    JLabel accountLabel,verTrekLabel,aankomstLabel,vervoerLabel,timeSpinnerLabel,usernameLabel,passwordLabel, passwordConfirmLabel;
    JPanel vertrekContainer = new JPanel(new GridLayout(0,1));
    JPanel aankomtContainer = new JPanel(new GridLayout(0,1));
    JPanel vervoersmiddelContainer = new JPanel(new GridLayout(0,1));
    JPanel timeSpinnerContainer = new JPanel(new GridLayout(0,1));
    JPanel switcherContainer = new JPanel(new GridLayout(0,1));
    JPanel zoekButtonContainer = new JPanel(new GridLayout(0,1));
    Border blackline = BorderFactory.createLineBorder(Color.BLACK);

    JPanel usernameContainer = new JPanel(new GridLayout(0,1));
    JPanel passwordContainer = new JPanel(new GridLayout(0,1));
    JPanel passWordConfirmContainer = new JPanel(new GridLayout(0,1));
    JPanel accountMakenContainer = new JPanel(new FlowLayout());
    JButton closeAccountMessage = new JButton("X");

    JTabbedPane TabbedPane(Locale locale, int userIdGiven) {
        userId = userIdGiven;
        bundle = ResourceBundle.getBundle("bundle", locale);
        reisPlanner();
        getLoginScreen();
        mainPanel = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.WRAP_TAB_LAYOUT);
        login = loginScreenView;
        reisplanner = reisplannerView;
        login.setVisible(true);
        reisplanner.setVisible(true);
        map.setVisible(true);
        mainPanel.addTab(bundle.getString("reisplanner"), reisplanner);
        mainPanel.addTab(bundle.getString("account"), login);
        //mainPanel.addTab(bundle.getString("map"), map);
        t1.setBackground(backGroundColorC);
        t2.setBackground(backGroundColorC);
        sT1.setBackground(backGroundColorC);
        sT2.setBackground(backGroundColorC);
        b1.setBackground(backGroundColor);
        b2.setBackground(backGroundColor);
        sB1.setBackground(backGroundColor);
        sB2.setBackground(backGroundColor);
        reisplannerView.setBackground(backGroundColorB);
        aankomstBox.setBackground(backGroundColor);
        vertrekBox.setBackground(backGroundColor);
        zoeken.setBackground(backGroundColor);
        tripsPanel.setBackground(backGroundColorB);
        vervoersmiddelContainer.setBackground(backGroundColorB);
        zoekButtonContainer.setBackground(backGroundColorB);
        vertrekContainer.setBackground(backGroundColorB);
        aankomtContainer.setBackground(backGroundColorB);
        switcherContainer.setBackground(backGroundColorB);
        timeSpinnerContainer.setBackground(backGroundColorB);
        vervoersmiddelContainer.setBorder(blackline);
        usernameContainer.setBackground(backGroundColorB);
        passwordContainer.setBackground(backGroundColorB);
        return mainPanel;
    }

    private void getLoginScreen(){
        loginScreenView = new JPanel(new FlowLayout());
        SignUp();
        account();

        loginScreen = new JPanel();
        loginScreen.setLayout(new GridLayout(0,1));
        loginScreen.setBorder(BorderFactory.createEmptyBorder(60, 30, 30, 30));


        //set the label login with a font and a color: Niels van Gortel
        l1 = new JLabel(bundle.getString("inloggen"));
        l1.setFont(new Font("Arial",Font.BOLD,30));
        l1.setForeground(Color.BLUE);

        // makes the textfield and buttons
        usernameContainer = new JPanel(new GridLayout(0,1));
        usernameLabel = new JLabel(bundle.getString("username"));
        t1 = new JTextField(40);
        usernameContainer.add(usernameLabel);
        usernameContainer.add(t1);
        usernameContainer.setBorder(new EmptyBorder(0,0,25,0));

        passwordContainer = new JPanel(new GridLayout(0,1));
        passwordLabel = new JLabel(bundle.getString("password"));
        t2 = new JPasswordField(40);
        passwordContainer.add(passwordLabel);
        passwordContainer.add(t2);
        passwordContainer.setBorder(new EmptyBorder(0,0,25,0));

        b1 = new JButton(bundle.getString("inloggen"));
        b2 = new JButton(bundle.getString("registreren"));
        l2 = new JLabel("");

        // add the textfield to the frame
        loginScreen.add(l1);
        loginScreen.add(usernameContainer);
        loginScreen.add(passwordContainer);
        // add the button to the frame
        loginScreen.add(b1);
        loginScreen.add(b2);
        // add the label to the frame
        loginScreen.add(l2);

        //Makes sure that the button login will make an action
        b1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                String username = t1.getText().toString();
                String password = t2.getText().toString();

                try {
                    int userIdGet = json.login(username,password);
                    userId =  userIdGet;
                    account();
                    if (userIdGet!=0){
                        matched = true;
                    }
                }catch (Exception e){}
                if(matched){
                    l2.setText("");
                    loginScreen.setVisible(false);
                    signUpScreen.setVisible(false);
//                    accountScreen = account(locale, userIdInstance);
                    accountScreen.setVisible(true);
                    loginScreenView.add(accountScreen);
                    loginScreenView.repaint();
                    loginScreenView.revalidate();
                    accountLabel.setText("");
                }else{
                    l2.setText(bundle.getString("verkeerdeGegevens"));
                }
            }
        });
        this.b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                signUpScreen.setVisible(true);
                loginScreen.setVisible(false);
            }
        });

        this.b2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                signUpScreen.setVisible(true);
                loginScreen.setVisible(false);
            }
        });

        loginScreenView.add(signUpScreen);
        loginScreenView.add(loginScreen);
        loginScreenView.add(accountScreen);

        loginScreen.setVisible(true);
        signUpScreen.setVisible(false);
        loginScreenView.setVisible(false);

//        if(matched){
//            l2.setText("");
//            loginScreen.setVisible(false);
//            signUpScreen.setVisible(false);
//            accountScreen.setVisible(true);
//            loginScreenView.add(accountScreen);
//            loginScreenView.revalidate();
//        }

        login = loginScreenView;
        loginScreenView.setBackground(backGroundColorB);
        loginScreen.setBackground(backGroundColorB);
        signUpScreen.setBackground(backGroundColorB);
        accountScreen.setBackground(backGroundColorB);

        if(userId!=0){
            matched = true;
            loginScreen.setVisible(false);
            signUpScreen.setVisible(false);
            accountScreen.setVisible(true);
        }else{
            matched = false;
        }
    }
    private void SignUp() {
        signUpScreen = new JPanel();
        signUpScreen.setLayout(new GridLayout(0,1));
        signUpScreen.setBorder(BorderFactory.createEmptyBorder(60, 30, 30, 30));

        //makes the textfield and button of the registration panel: Niels van Gortel
        usernameContainer = new JPanel(new GridLayout(0,1));
        usernameLabel = new JLabel(bundle.getString("username"));
        sT1 = new JTextField(40);
        usernameContainer.add(usernameLabel);
        usernameContainer.add(sT1);
        usernameContainer.setBackground(backGroundColorB);
        usernameContainer.setBorder(new EmptyBorder(0,0,20,0));

        passwordContainer = new JPanel(new GridLayout(0,1));
        passwordLabel = new JLabel(bundle.getString("password"));
        sT2 = new JPasswordField(40);
        passwordContainer.add(passwordLabel);
        passwordContainer.add(sT2);
        passwordContainer.setBackground(backGroundColorB);
        passwordContainer.setBorder(new EmptyBorder(0,0,20,0));
        //Confirm password
        passWordConfirmContainer = new JPanel(new GridLayout(0,1));
        passwordConfirmLabel = new JLabel("Confirm password");
        sT3 = new JPasswordField(40);
        sT3.setBackground(backGroundColorC);
        passWordConfirmContainer.add(passwordConfirmLabel);
        passWordConfirmContainer.add(sT3);
        passWordConfirmContainer.setBackground(backGroundColorB);
        passWordConfirmContainer.setBorder(new EmptyBorder(0,0,20,0));
        //

        sB1 = new JButton(bundle.getString("bevestig"));
        sB2 = new JButton(bundle.getString("goBack"));

        sL3 = new JLabel(bundle.getString("registratie"));
        sL3.setFont(new Font("Arial", Font.BOLD, 30));
        sL3.setForeground(Color.BLUE);

        sL4 = new JLabel();
        sL4.setFont(new Font("Arial", Font.BOLD, 10));
        sL4.setForeground(Color.RED);

        sB1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                try {
                    List <User> users = json.getUsers();
                    List <String> userNames = new ArrayList<String>();
                    for (User u :users){
                        userNames.add(u.getUserName());
                    }
//                    System.out.println(userNames);
                    if (userNames.contains(sT1.getText())) {
                        sL4.setText(bundle.getString("userTaken"));
                    } else {
                        if(sT1.getText().length()!=0 && sT2.getText().length()!=0 && sT3.getText().length()!=0) {
                            if (sT2.getText().equals(sT3.getText())){
                                ArrayList<UserTrip> userTrips = new ArrayList<>();
                                var user = new User(0, sT1.getText(), sT2.getText(), userTrips);
                                json.addUser(user);
                                Frame f = new JFrame();
                                JOptionPane.showMessageDialog(f, bundle.getString("accountAangemaakt"));
                                signUpScreen.setVisible(false);
                                loginScreen.setVisible(true);
                                accountLabel.setText("");
                                sL4.setText("");
                                sT1.setText("");
                                sT2.setText("");
                            }else {
                                sL4.setText(bundle.getString("wwNoMatch"));
                            }
                        }else {
                            sL4.setText(bundle.getString("onjuistInvoer"));
                        }
                    }
                } catch (Exception e) {
                }
            }
        });
        sB2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                signUpScreen.setVisible(false);
                loginScreen.setVisible(true);
                sT1.setText("");
                sT2.setText("");
                mainPanel.revalidate();
                mainPanel.repaint();
            }
        });

        signUpScreen.add(sL3);
        signUpScreen.add(usernameContainer);
        signUpScreen.add(passwordContainer);
        signUpScreen.add(passWordConfirmContainer);
        signUpScreen.add(sB1);
        signUpScreen.add(sB2);
        signUpScreen.add(sL4);
        signUpScreen.setVisible(false);
    }
    private void account(){
        accountScreen = new JPanel();
        accountScreen.setLayout(new BorderLayout());
        accountScreen.setPreferredSize(new Dimension(950, 750));

        userTripLoader();
        user = json.getUser(userId);
        JLabel userName = new JLabel(user.getUserName());
        userName.setFont(new Font("Arial",Font.BOLD,30));
        userName.setBorder(thickColoredBorder);

        var userTripsScrollPanel = new JScrollPane(uTripsPanel);
        userTripsScrollPanel.setPreferredSize(new Dimension(950, 400));
        userTripsScrollPanel.getVerticalScrollBar().setBackground(backGroundColorC);

        JButton logOut = new JButton(bundle.getString("logout"));
        logOut.setBackground(backGroundColor);
        logOut.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                userId = 0;
                matched = false;
                zoeken.doClick();
                loginScreenView.remove(accountScreen);
                accountScreen.setVisible(false);
                signUpScreen.setVisible(false);
                loginScreen.setVisible(true);
                mainPanel.revalidate();
                mainPanel.repaint();
            }
        });

        var accountContainer = new JPanel(new BorderLayout());
        accountContainer.add(userName, BorderLayout.WEST);
        accountContainer.add(logOut, BorderLayout.EAST);
        accountContainer.setBackground(backGroundColorB);
        var containerContainer = new JPanel(new BorderLayout());

        var favLabel = new JLabel(bundle.getString("favorieten"));
        favLabel.setFont(new Font("Arial",Font.BOLD,40));
        favLabel.setBorder(BorderFactory.createEmptyBorder(50, 200, 5, 200));
        containerContainer.add(accountContainer, BorderLayout.NORTH);
        containerContainer.add(favLabel, BorderLayout.SOUTH);
        containerContainer.setBackground(backGroundColorB);
        accountScreen.add(containerContainer, BorderLayout.NORTH);
        accountScreen.add(userTripsScrollPanel, BorderLayout.CENTER);
        accountScreen.setVisible(false);
        loginScreenView.add(accountScreen);
    }
    private void userTripLoader(){
        uTripsPanel = new JPanel();
        uTripsPanel.setLayout(new GridLayout(0,1));
        uTripsPanel.setBorder(thickColoredBorder);
        uTripsPanel.setBackground(backGroundColorB);
        for (UserTrip t: json.getUser(userId).getTrips()){
            JPanel utripPanel = new JPanel(new FlowLayout());
            utripPanel.setBorder(black);
            var departure = new JLabel(bundle.getString("vertrekLocatie")+" : " + t.getDeparture()+ "     ");
            var arrival = new JLabel(bundle.getString("aankomstLocatie")+" : " + t.getArrival() + "     ");
            var delete = new JButton(bundle.getString("delFromFav"));
            delete.setBackground(backGroundColor);

            utripPanel.add(departure, BorderLayout.WEST);
            utripPanel.add(arrival, BorderLayout.CENTER);
            utripPanel.add(delete,BorderLayout.EAST);
            utripPanel.setMaximumSize(new Dimension(730, 50));
            utripPanel.setBackground(backGroundColorB);

            var lookUp = new JButton(bundle.getString("planTrip"));
            lookUp.setBackground(backGroundColor);
            utripPanel.add(lookUp, BorderLayout.SOUTH);

            search = new JButton();
            utimeSpinner = new javax.swing.JSpinner();
            lookUp.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    utripPanel.remove(search);
                    utripPanel.remove(utimeSpinner);
                    Date date = new Date();
                    SpinnerDateModel sm = new SpinnerDateModel(date, null, null, Calendar.HOUR_OF_DAY);

                    utimeSpinner = new javax.swing.JSpinner(sm);
                    JSpinner.DateEditor de = new JSpinner.DateEditor(utimeSpinner, "HH:mm");
                    utimeSpinner.setEditor(de);
                    utimeSpinner.setBackground(backGroundColor);

                    search = new JButton(bundle.getString("zoeken"));
                    search.setBackground(backGroundColor);
                    utripPanel.add(timeSpinner);
                    utripPanel.add(search);
                    accountScreen.revalidate();
                    search.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                            departureTimeSearch = sdf.format(timeSpinner.getValue());
                            LocalTime localTimeA = LocalTime.parse(departureTimeSearch);
                            departureSearch = t.getDeparture();
                            arrivalSearch = t.getArrival();
                            localTimeD = localTimeA;
                            System.out.println(departureSearch+arrivalSearch+localTimeA);
                            ///Has to change!!!!1
                            //Should be
                            //userSearch = t.getModeOfTransport
                            userSearch = bundle.getString("trein");
                            zoeken.doClick();
                            mainPanel.setSelectedIndex(0);
                            accountScreen.revalidate();
                            userSearch = "";
                        }
                    });
                }
            });

            delete.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    json.deleteUserTrip(userId, t.getTripId());
                    uTripsPanel.remove(utripPanel);
                    utripPanel.setVisible(false);
                    mainPanel.revalidate();
                    mainPanel.repaint();
                }
            });
            uTripsPanel.add(utripPanel);
        }

        userTripsScrollPanel = new JScrollPane(uTripsPanel);
    }

    private void reisPlanner(){
        reisplannerView = new JPanel();
        zoeken = new JButton(bundle.getString("zoeken"));
        rL = new JLabel();

        {
            // comboboxen DOOR: Niels van Gortel
            JPanel comboBoxPanel = new JPanel();
            comboBoxPanel.setBounds(50, 100, 394, 25);
            comboBoxPanel.setLayout(new GridLayout(0, 1, 10, 0));
            comboBoxPanel.setBackground(backGroundColorB);

            JPanel transport = new JPanel();
            transport.setBounds(10, 3, 10, 25);
            transport.setBackground(backGroundColorB);

            Date date = new Date();
            SpinnerDateModel sm = new SpinnerDateModel(date, null, null, Calendar.HOUR_OF_DAY);

            timeSpinner = new javax.swing.JSpinner(sm);
            timeSpinner.setBounds(500, 500, 100, 25);
            JSpinner.DateEditor de = new JSpinner.DateEditor(timeSpinner, "HH:mm");
            timeSpinner.setEditor(de);

            reisplannerView.add(comboBoxPanel);

            // Zet het keuze menu van de vertreklocaties

            vertrekBox = new JComboBox();
            aankomstBox = new JComboBox();

            // maakt het panel aan
            tripsPanel = new JPanel();
            scrollPane = new JScrollPane();
            //sets the choice menu of the transportation
            JRadioButton train = new JRadioButton(bundle.getString("trein"));
            JRadioButton tram = new JRadioButton(bundle.getString("tram"));
            JRadioButton bus = new JRadioButton(bundle.getString("bus"));

            JButton switcher = new JButton(bundle.getString("switch"));
            switcher.setBackground(backGroundColor);

            JFrame mapframe = new JFrame();
            JButton map = new JButton("Map");

            // makes sure that one of the buttons can be selected
            train.setBackground(backGroundColorB);
            bus.setBackground(backGroundColorB);
            tram.setBackground(backGroundColorB);
            group.add(train);
            group.add(bus);
            group.add(tram);

            transport.add(train);
            transport.add(tram);
            transport.add(bus);

            //Labels
            timeSpinnerLabel = new JLabel(bundle.getString("vertrekTijd"));
            verTrekLabel = new JLabel(bundle.getString("vertrekLocatie"));
            aankomstLabel = new JLabel(bundle.getString("aankomstLocatie"));
            vervoerLabel = new JLabel(bundle.getString("vervoersmiddel"));
            vertrekContainer = new JPanel(new GridLayout(0,1));
            verTrekLabel = new JLabel("Vertrek locatie");
            vervoersmiddelContainer.add(vervoerLabel);
            vervoersmiddelContainer.add(transport);
            timeSpinnerContainer.add(timeSpinnerLabel);
            timeSpinnerContainer.add(timeSpinner);
            vertrekContainer.add(verTrekLabel);
            vertrekContainer.add(vertrekBox);
            aankomtContainer.add(aankomstLabel);
            aankomtContainer.add(aankomstBox);
            aankomtContainer.setVisible(false);
            switcherContainer.add(switcher);
            switcherContainer.setBorder(new EmptyBorder(27,0,0,0));
            zoekButtonContainer.add(zoeken);
            zoekButtonContainer.setBorder(new EmptyBorder(27,0,0,0));
            // Voegt attributen toe aan het panel.

            JPanel zoekContainer = new JPanel(new FlowLayout());
            zoekContainer.add(vervoersmiddelContainer);
            zoekContainer.add(timeSpinnerContainer);
            zoekContainer.add(vertrekContainer);
//            zoekContainer.add(switcher);
            zoekContainer.add(switcherContainer);
            zoekContainer.add(aankomtContainer);
//            zoekContainer.add(zoeken);
            zoekContainer.add(zoekButtonContainer);
            zoekContainer.add(rL);
//            zoekContainer.setPreferredSize(new Dimension(680, 100));
            zoekContainer.setBorder(new EmptyBorder(0,0,80,0));
            zoekContainer.setBackground(backGroundColorB);
            reisplannerView.add(zoekContainer);

            reisplannerView.setVisible(false);
            switcherContainer.setVisible(false);
            map.setVisible(true);

            HashMap<String, Location> locations = trainData.getLocationMap();
            Set<String> keySet = locations.keySet();
            ArrayList<String> listOfKeys = new ArrayList<String>(keySet);

            train.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    vertrekContainer.remove(vertrekBox);
                    aankomtContainer.remove(aankomstBox);
                    vertrekBox = new JComboBox(new ArrayList<String>(trainData.getLocationMap().keySet()).toArray());
                    aankomstBox = new JComboBox(new ArrayList<String>(trainData.getLocationMap().keySet()).toArray());
                    vertrekBox.setBackground(backGroundColor);
                    aankomstBox.setBackground(backGroundColor);
                    aankomstBox.setSelectedIndex(1);
                    vertrekContainer.add(vertrekBox);
                    aankomtContainer.add(aankomstBox);
                    mainPanel.revalidate();
                    mainPanel.repaint();
                    vertrekBox.addActionListener(event -> {
                        aankomtContainer.remove(aankomstBox);
                        var listOfKeysB = new ArrayList<String>(trainData.getLocationMap().keySet());
                        listOfKeysB.remove(vertrekBox.getSelectedItem());
                        aankomstBox = new JComboBox(listOfKeysB.toArray());
                        aankomstBox.setBackground(backGroundColor);
                        aankomtContainer.add(aankomstBox);
                        aankomtContainer.setVisible(true);
                        switcherContainer.setVisible(true);
                        mainPanel.revalidate();
                    });
                    aankomstBox.addActionListener(event -> {
//                        zoekContainer.remove(vertrekBox);
                        vertrekContainer.remove(vertrekBox);
                        var listOfKeysB = new ArrayList<String>(trainData.getLocationMap().keySet());
                        listOfKeysB.remove(vertrekBox.getSelectedItem());
                        vertrekBox = new JComboBox(listOfKeysB.toArray());
                        vertrekBox.setBackground(backGroundColor);
                        vertrekContainer.add(vertrekBox);
//                        zoekContainer.add(vertrekBox);
//                        zoekContainer.remove(zoeken);
//                        zoekContainer.add(zoeken);
                        mainPanel.revalidate();
                        mainPanel.repaint();
                    });
                }
            });

            bus.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    vertrekContainer.remove(vertrekBox);
                    aankomtContainer.remove(aankomstBox);
                    vertrekBox = new JComboBox(new ArrayList<String>(busData.getLocationMap().keySet()).toArray());
                    aankomstBox = new JComboBox(new ArrayList<String>(busData.getLocationMap().keySet()).toArray());
                    vertrekBox.setBackground(backGroundColor);
                    aankomstBox.setBackground(backGroundColor);
                    vertrekContainer.add(vertrekBox);
                    aankomtContainer.add(aankomstBox);
                    mainPanel.revalidate();
                    mainPanel.repaint();
                    vertrekBox.addActionListener(event -> {
                        aankomtContainer.remove(aankomstBox);
                        var listOfKeysB = new ArrayList<String>(busData.getLocationMap().keySet());
                        listOfKeysB.remove(vertrekBox.getSelectedItem());
                        aankomstBox = new JComboBox(listOfKeysB.toArray());
                        aankomstBox.setBackground(backGroundColor);
                        aankomtContainer.add(aankomstBox);
                        aankomtContainer.setVisible(true);
                        switcherContainer.setVisible(true);
                        mainPanel.revalidate();
                    });
                    aankomstBox.addActionListener(event -> {
//                        zoekContainer.remove(vertrekBox);
                        vertrekContainer.remove(vertrekBox);
                        var listOfKeysB = new ArrayList<String>(busData.getLocationMap().keySet());
                        listOfKeysB.remove(vertrekBox.getSelectedItem());
                        vertrekBox = new JComboBox(listOfKeysB.toArray());
                        vertrekBox.setBackground(backGroundColor);
                        vertrekContainer.add(vertrekBox);
//                        zoekContainer.add(vertrekBox);
//                        zoekContainer.remove(zoeken);
//                        zoekContainer.add(zoeken);
                        mainPanel.revalidate();
                        mainPanel.repaint();
                    });
                }
            });
            tram.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    vertrekContainer.remove(vertrekBox);
                    aankomtContainer.remove(aankomstBox);
                    vertrekBox = new JComboBox();
                    aankomstBox = new JComboBox();
                    vertrekBox.setBackground(backGroundColor);
                    aankomstBox.setBackground(backGroundColor);
                    vertrekContainer.add(vertrekBox);
                    aankomtContainer.add(aankomstBox);
                    mainPanel.revalidate();
                    mainPanel.repaint();
                }
            });
            switcher.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    vertrekContainer.remove(vertrekBox);
                    aankomtContainer.remove(aankomstBox);
                    ArrayList<String> listOfKeysB = listOfKeys;
                    var temp = vertrekBox;
                    vertrekBox = aankomstBox;
                    aankomstBox = temp;
                    vertrekContainer.add(vertrekBox);
                    aankomtContainer.add(aankomstBox);
                    mainPanel.revalidate();
                    mainPanel.repaint();
                }
            });

        }
        scrollPane = new JScrollPane();
        // Zorgt ervoor dat er een response komt als de zoek button ingedrukt wordt.
        zoeken.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent search) {
                int i = 0;
                reisplannerView.remove(tripScrollContainer);
                if (userSearch.equals("")){
                    arrivalSearch = (String) aankomstBox.getSelectedItem();
                    departureSearch = (String) vertrekBox.getSelectedItem();
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                    departureTimeSearch = sdf.format(timeSpinner.getValue());
                    localTimeD = LocalTime.parse(departureTimeSearch);
                }

                reisplannerView.remove(scrollPane);
                reisplannerView.remove(scrollPane);
                reisplannerView.remove(tripsPanel);
                reisplannerView.remove(selectTransport);
                reisplannerView.remove(tripsPanel);
                tripsPanel = new JPanel();
                tripsPanel.setLayout(new GridLayout(0, 1));

                if (getSelectedButton() != null && getSelectedButton().equals(bundle.getString("trein"))) {
                    Trips trips = trainData.getTrips(departureSearch, arrivalSearch, localTimeD);

                    ArrayList<Trip> tripArrayList = trips.getTrips();
                    //TripsBefore are the trips before the preferred departure time, tripsAfter are the trips after the preferred time
                    ArrayList<Trip> tripsBefore = new ArrayList<>();
                    ArrayList<Trip> tripsAfter = new ArrayList<>();

                    Collections.sort(tripArrayList, new Comparator<Trip>() {
                        public int compare(Trip t1, Trip t2) {
                            return t1.getDeparture().compareTo(t2.getDeparture());
                        }
                    });

                    //Populates the trips arrays
                    for (Trip t : tripArrayList) {
                        if (t.getDeparture().isAfter(localTimeD)) {
                            tripsAfter.add(t);
                        }else if (t.getDeparture().isBefore(localTimeD)){
                            tripsBefore.add(t);
                        }
                    }
//                    Prints the trips
                    if (tripsBefore.size()>0){
                        tripPlanner(tripsBefore.get(tripsBefore.size()-1));
                    }
                    for (Trip t: tripsAfter){
                        tripPlanner(t);
                    }

                    if (tripArrayList.size() == 0) {
                        var tripButton = new JButton();
                        tripButton.setText(bundle.getString("geenReis"));
                        tripButton.setPreferredSize(new Dimension(235, 20));
                        tripsPanel.add(tripButton);
                    }

                }

                if(getSelectedButton() != null && getSelectedButton().equals(bundle.getString("bus"))){
                    Trips trips = busData.getTrips(departureSearch, arrivalSearch, localTimeD);

                    ArrayList<Trip> tripArrayList = trips.getTrips();
                    //TripsBefore are the trips before the preferred departure time, tripsAfter are the trips after the preferred time
                    ArrayList<Trip> tripsBefore = new ArrayList<>();
                    ArrayList<Trip> tripsAfter = new ArrayList<>();

                    Collections.sort(tripArrayList, new Comparator<Trip>() {
                        public int compare(Trip t1, Trip t2) {
                            return t1.getDeparture().compareTo(t2.getDeparture());
                        }
                    });

                    //Populates the trips arrays
                    for (Trip t : tripArrayList) {
                        if (t.getDeparture().isAfter(localTimeD)) {
                            tripsAfter.add(t);
                        }else if (t.getDeparture().isBefore(localTimeD)){
                            tripsBefore.add(t);
                        }
                    }
//                    Prints the trips
                    if (tripsBefore.size()>0){
                        tripPlanner(tripsBefore.get(tripsBefore.size()-1));
                    }
                    for (Trip t: tripsAfter){
                        tripPlanner(t);
                    }

                    if (tripArrayList.size() == 0) {
                        var tripButton = new JButton();
                        tripButton.setText(bundle.getString("geenReis"));
                        tripButton.setPreferredSize(new Dimension(235, 20));
                        tripsPanel.add(tripButton);
                    }
                }

                else {
                    selectTransport.setText(bundle.getString("selectVervoer"));
                    reisplannerView.add(selectTransport);
                }
                
                scrollPane = new JScrollPane(tripsPanel);
                scrollPane.setPreferredSize(new Dimension(650, 600));
                scrollPane.getVerticalScrollBar().setUnitIncrement(20);
                scrollPane.getVerticalScrollBar().setBackground(backGroundColorC);

                tripScrollContainer = new JPanel(new BorderLayout());
                var tripsLabel = new JLabel("Trips");
                tripsLabel.setFont(new Font("Arial",Font.BOLD,40));
                tripsLabel.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
                tripScrollContainer.add(tripsLabel, BorderLayout.NORTH);
                tripScrollContainer.add(scrollPane, BorderLayout.CENTER);
                tripScrollContainer.setBackground(backGroundColorC);
                tripScrollContainer.setBorder(black);
                reisplannerView.add(tripScrollContainer);
                reisplannerView.revalidate();
            }
        });
        reisplannerView.revalidate();
        reisplannerView.repaint();
    }

    private String getSelectedButton() {
        for (Enumeration<AbstractButton> buttons = group.getElements(); buttons.hasMoreElements();) {
            AbstractButton button = buttons.nextElement();
            if (button.isSelected()) {
                return button.getText();
            }
        }
        return null;
    }

    public void tripPlanner(Trip t){
        loginScreen.remove(accountMakenContainer);

        Duration duration = Duration.between(t.getDeparture(), t.getArrival());
        var tripText = new JLabel();
        tripText.setFont(new Font("Arial", Font.BOLD, 14));
        tripText.setText(departureSearch + " " + t.getDeparture() + " -> " + arrivalSearch + " " + t.getArrival());

        var tripDuration = new JLabel();
        tripDuration.setFont(new Font("Arial", Font.BOLD, 9));
        tripDuration.setBorder(new EmptyBorder(0, 25, 0, 10));
        tripDuration.setText(bundle.getString("duration") + ": " + String.valueOf(duration.toMinutes()));

        var stopOvers = new JLabel();
        stopOvers.setFont(new Font("Arial", Font.BOLD, 9));
        stopOvers.setBorder(new EmptyBorder(0, 25, 0, 10));
        stopOvers.setText(bundle.getString("stops") + " " + t.getRoute().getStopOvers(departureSearch, arrivalSearch));

        var transportType = new JLabel();
        transportType.setFont(new Font("Arial", Font.BOLD, 14));
        transportType.setBorder(new EmptyBorder(0, 25, 0, 10));
        transportType.setText(getSelectedButton());


        String[] columnNames = {bundle.getString("station"), bundle.getString("aankomstTijd"), bundle.getString("vertrekTijd")};
        DefaultTableModel model = new DefaultTableModel(columnNames, 0);
        for (StopOver sOver : t.getRoute().getStopOversArray()) {
            String name = sOver.getName();
            String arrival = sOver.getArrival().toString();
            String departure = sOver.getDeparture().toString();

            String[] row = {name, departure, arrival};
            model.addRow(row);
        }
        JTable table = new JTable(model);

        JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.add(table, BorderLayout.CENTER);
        tablePanel.add(table.getTableHeader(), BorderLayout.NORTH);
        tablePanel.setVisible(false);

        var tripPanel = new JPanel();
        tripPanel.setBorder(blackline);
        tripPanel.setLayout(new BorderLayout());

        if (userId != 0) {
            var addToHistory = new JButton(bundle.getString("toevoegen"));
            addToHistory.setFont(new Font("Arial", Font.BOLD, 10));
            addToHistory.setBackground(backGroundColorC);
            addToHistory.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    json.addUserTrip(userId, departureSearch, arrivalSearch, getSelectedButton());
                    zoeken.doClick();
                    loginScreenView.remove(accountScreen);
                    account();
                    accountScreen.setVisible(true);
                    loginScreenView.add(accountScreen);
                    loginScreenView.revalidate();
                }
            });
            for(UserTrip uT :json.getUser(userId).getTrips()){
                if (uT.getDeparture().equals(t.getLocationA().getName()) &&  uT.getArrival().equals(t.getLocationB().getName())){
                    addToHistory = new JButton(bundle.getString("delFromFav"));
                    addToHistory.setFont(new Font("Arial", Font.BOLD, 10));
                    addToHistory.setBackground(backGroundColorC);
                    addToHistory.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            json.deleteUserTripByTrip(userId, t);
                            zoeken.doClick();
                            loginScreenView.remove(accountScreen);
                            account();
                            accountScreen.setVisible(true);
                            loginScreenView.add(accountScreen);
                            loginScreenView.revalidate();
                        }
                    });
                }
            }
            tripPanel.add(addToHistory, BorderLayout.EAST);
        } else {
            if (userId == 0){
                var addToHistory = new JButton(bundle.getString("maakAccount"));
                addToHistory.setFont(new Font("Arial", Font.BOLD, 9));
                addToHistory.setBackground(backGroundColorC);
                addToHistory.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        mainPanel.setSelectedIndex(1);
                        accountLabel = new JLabel(bundle.getString("makeAccount"));
                        accountMakenContainer = new JPanel(new FlowLayout());
                        closeAccountMessage = new JButton("X");
                        closeAccountMessage.setBackground(backGroundColor);

                        closeAccountMessage.addActionListener(new ActionListener() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                               loginScreen.remove(accountMakenContainer);
                               mainPanel.revalidate();
                               mainPanel.repaint();
                            }
                        });

                        accountMakenContainer.add(accountLabel);
                        accountMakenContainer.add(closeAccountMessage);
                        accountMakenContainer.setBackground(backGroundColorC);
                        accountMakenContainer.setBorder(thickColoredBorder);
                        loginScreen.add(accountMakenContainer);
                        mainPanel.revalidate();
                    }
                });
                tripPanel.add(addToHistory, BorderLayout.EAST);
            }

        }

        var tripInfo = new JButton();
        tripInfo.setText(bundle.getString("routeInfo"));

        tripInfo.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tableVisible == true) {
                    tableVisible = false;
                    tablePanel.setVisible(false);
                } else {
                    tableVisible = true;
                    tablePanel.setVisible(true);
                }
            }
        });

        JButton info = new JButton();
        info.setText(bundle.getString("stationInformatie"));

        var labelContainer = new JPanel(new BorderLayout());
        var moreButtons = new JPanel(new BorderLayout());
        labelContainer.add(transportType, BorderLayout.WEST);
        labelContainer.add(tripDuration, BorderLayout.CENTER);
        labelContainer.add(stopOvers, BorderLayout.EAST);
        labelContainer.add(moreButtons, BorderLayout.SOUTH);
        moreButtons.add(tripInfo, BorderLayout.WEST);
        moreButtons.add(info, BorderLayout.CENTER);

        info.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (stationInfo == true) {
                    stationInfo = false;
                    info.setText(bundle.getString("WC") + "        SMULLERS        AH TO GO        OV Service");
                } else {
                    stationInfo = true;
                    info.setText(bundle.getString("stationInformatie"));
                }
            }
        });


        tripPanel.add(tripText, BorderLayout.WEST);
        tripPanel.add(labelContainer, BorderLayout.SOUTH);
        tripsPanel.add(tripPanel);
        tripsPanel.add(tablePanel);
        tripsPanel.setVisible(true);
        tripsPanel.setBackground(backGroundColorB);
        table.setBackground(backGroundColorB);
        tripPanel.setBackground(backGroundColor);
        labelContainer.setBackground(backGroundColorB);
        moreButtons.setBackground(backGroundColorB);
        info.setBackground(backGroundColor);
        tripInfo.setBackground(backGroundColor);
        tripsPanel.setBorder(thickColoredBorder);
    }
}