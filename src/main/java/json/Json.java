package json;

import com.fasterxml.jackson.core.exc.StreamReadException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DatabindException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import objects.Trip;
import objects.UserTrip;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Json {
    private ObjectMapper mapper = new ObjectMapper();

    public void addUser(User user){
        try {
            mapper.registerModule(new JavaTimeModule());
            InputStream inputStream = new FileInputStream(new File("src/main/resources/jsonFiles/users.json"));
            var typeReference = new TypeReference<List<User>>() {};
            List<User> users = mapper.readValue(inputStream,typeReference);
            user.setUserId(users.size());
            users.add(user);
            mapper.writeValue(new File("src/main/resources/jsonFiles/users.json"), users);
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int login(String username, String password){
        int userId = 0;
        try {
            mapper.registerModule(new JavaTimeModule());
            InputStream inputStream = new FileInputStream(new File("src/main/resources/jsonFiles/users.json"));
            var typeReference = new TypeReference<List<User>>() {};
            List<User> users = mapper.readValue(inputStream,typeReference);
            for (User u : users){
                if(username.equals(u.getUserName()) && password.equals(u.getPassword())){
                    userId = u.getUserId();
                }
            }
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return userId;
    }

    public User getUser(int userId){
        User user = null;
        try {
            mapper.registerModule(new JavaTimeModule());
            InputStream inputStream = new FileInputStream(new File("src/main/resources/jsonFiles/users.json"));
            var typeReference = new TypeReference<List<User>>() {};
            List<User> users = mapper.readValue(inputStream,typeReference);
            user = users.get(userId);
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return user;
    }
    public List getUsers(){
        List<User> users = new ArrayList<User>();
        try {
            mapper.registerModule(new JavaTimeModule());
            InputStream inputStream = new FileInputStream(new File("src/main/resources/jsonFiles/users.json"));
            var typeReference = new TypeReference<List<User>>() {};
            users = mapper.readValue(inputStream,typeReference);
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return users;
    }
    public void addUserTrip(int userId, String departure, String arrival, String modeOfTransport){
        try {
            mapper.registerModule(new JavaTimeModule());
            InputStream inputStream = new FileInputStream(new File("src/main/resources/jsonFiles/users.json"));
            var typeReference = new TypeReference<List<User>>() {};
            List<User> users = mapper.readValue(inputStream,typeReference);
            users.get(userId).getTrips().add(new UserTrip(getUser(userId).getTrips().size(), departure, arrival, modeOfTransport));
            mapper.writeValue(new File("src/main/resources/jsonFiles/users.json"), users);
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void deleteUserTrip(int userId, int tripId){
        try {
            mapper.registerModule(new JavaTimeModule());
            InputStream inputStream = new FileInputStream(new File("src/main/resources/jsonFiles/users.json"));
            var typeReference = new TypeReference<List<User>>() {};
            List<User> users = mapper.readValue(inputStream,typeReference);
            users.get(userId).getTrips().removeIf(UserTrip -> (UserTrip.getTripId()==tripId));
            mapper.writeValue(new File("src/main/resources/jsonFiles/users.json"), users);
            inputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserTripByTrip(int userId, Trip trip){
        try {
            mapper.registerModule(new JavaTimeModule());
            InputStream inputStream = new FileInputStream(new File("src/main/resources/jsonFiles/users.json"));
            var typeReference = new TypeReference<List<User>>() {};
            List<User> users = mapper.readValue(inputStream,typeReference);
            users.get(userId).getTrips().removeIf(UserTrip -> (UserTrip.getDeparture().equals(trip.getLocationA().getName()) &&  UserTrip.getArrival().equals(trip.getLocationB().getName())));
            mapper.writeValue(new File("src/main/resources/jsonFiles/users.json"), users);
            inputStream.close();
            System.out.println("Successfully deleted trip");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (StreamReadException e) {
            e.printStackTrace();
        } catch (DatabindException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
