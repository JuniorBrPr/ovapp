package json;


import objects.Trip;
import objects.UserTrip;

import java.util.ArrayList;

public class User {
    private int userId;
    private String userName;
    private String password;
    private ArrayList<UserTrip> trips;


    public User() {
    }

    public User(int userId, String userName, String password, ArrayList<UserTrip> trips) {
        this.userId = userId;
        this.userName = userName;
        this.password = password;
        this.trips = new ArrayList<>();
    }

    public ArrayList<UserTrip> getTrips() {
        return trips;
    }

    public void setTrips(ArrayList<UserTrip> trips) {
        this.trips = trips;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
